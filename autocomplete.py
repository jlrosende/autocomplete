import sys
import re
class Node():
    def __init__(self, value):
        self.value = value
        self.right = None
        self.left = None

class AVLTree():

    def __init__(self):
        self.root = None

    def insert(self, value):
        if len(value) > 0:
            self.root = self.insertTree(self.root, value.lower())
    
    def insertTree(self, root, value):
        if not root:
            root = Node(value)
            return root
        if root.value == value:
            return root 
        elif root.value < value:
            root.right = self.insertTree(root.right, value)
        else:
            root.left = self.insertTree(root.left, value)

        # get Balance
        balance = self.getBalance(root)

        # Case 1 - Left Left 
        if balance > 1 and value < root.left.value: 
            return self.rightRotate(root) 
  
        # Case 2 - Right Right 
        if balance < -1 and value > root.right.value: 
            return self.leftRotate(root) 
  
        # Case 3 - Left Right 
        if balance > 1 and value > root.left.value: 
            root.left = self.leftRotate(root.left) 
            return self.rightRotate(root) 
  
        # Case 4 - Right Left 
        if balance < -1 and value < root.right.value: 
            root.right = self.rightRotate(root.right) 
            return self.leftRotate(root) 

        return root


    def leftRotate(self, z): 
        """
        Left rotate nodes 
        """
        y = z.right 
        T2 = y.left 
  
        # Perform rotation 
        y.left = z 
        z.right = T2 
  
        # Return the new root 
        return y 
  
    def rightRotate(self, z): 
        """
        Right rotate nodes 
        """
        y = z.left 
        T3 = y.right 
  
        # Perform rotation 
        y.right = z 
        z.left = T3 
  
        # Return the new root 
        return y 

    def getHeight(self, root): 
        """
        Get the height of the tree
        """
        if not root: 
            return 0
  
        leftHeight = self.getHeight(root.left)
        rightHeight = self.getHeight(root.right)
        if leftHeight > rightHeight: 
            return leftHeight + 1
        else:
            return rightHeight + 1

    def getBalance(self, root): 
        """
        Get the balance of the tree branches
        """
        if not root: 
            return 0
  
        return self.getHeight(root.left) - self.getHeight(root.right)

    def search(self, prefix):
        """
        Search a list of words with a prefix
        """
        values = []
        self.searchTree(self.root, prefix, values)
        return values

    def searchTree(self, root, prefix, values):
        """
        Search values with pefix 
        """
        if not root:
            return

        if root.value.startswith(prefix): # If the current node have the prefix search in both branches
            values.append(root.value)
            self.searchTree(root.left, prefix, values)
            self.searchTree(root.right, prefix, values)
        else: # Search in only one branch
            if root.left and root.value > prefix:
                self.searchTree(root.left, prefix, values)
            elif root.right and root.value < prefix:
                self.searchTree(root.right, prefix, values)



if __name__ == "__main__":
    

    if len(sys.argv) < 3:
        print("Usage: autocomplete <file> <prefix>")
        print("the <file> must have a list of words separated by commas")
        exit(0)

    with open(sys.argv[1], 'r') as f:
        contents = f.read()
    
    tree = AVLTree()
    for word in contents.split(","):
        tree.insert(word)

    word_list = tree.search(sys.argv[2])
    print(word_list, file=sys.stdout)
