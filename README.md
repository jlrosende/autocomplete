# Autocomplete

This program reads the words from a file and returns a list of words that begin with a given prefix.

Usage:

```bash
autocomplete <file> <prefix>
```

## Data Structure

The words read from the file are stored in a balanced binary search tree AVL.

Duplicate words are not added.

The words are sorted automatically thanks to the tree structure, which maintains the balance allowing an optimal search

## Search Algorithm

It is started looking for the root of the tree, if this contains the prefix it is added and it is continually looking for for the two daughter branches, in case that it does not contain the prefix, it is verified that ordination has the prefix for that node, and it is continued the search for that branch, discarding the opposite one.

the search system has an O(n) notation being the worst case, that all words have the prefix passed by parameter
